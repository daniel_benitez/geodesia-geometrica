import math

a = 6378388
e = 0.00612267

print("Ingresa la latitud (φ) dada en el ejercicio: ")
        
grados_latitud = float(input("Ingresa los grados (°): "))
minutos_latitud = float(input("Ingresa los minutos ('): "))
segundos_latitud = float(input("Ingresa los segundos (''): "))
        
minutos_decimal_latitud = minutos_latitud / 60
segundos_decimal_latitud = segundos_latitud / 3600
latitud_decimal = grados_latitud + minutos_decimal_latitud + segundos_decimal_latitud
        
latitud_radianes = latitud_decimal * (math.pi / 180)

print("Ingresa el azimut (α) dado en el ejercicio: ")
        
grados_azimut = float(input("Ingresa los grados (°): "))
minutos_azimut = float(input("Ingresa los minutos ('): "))
segundos_azimut = float(input("Ingresa los segundos (''): "))
        
minutos_decimal_azimut = minutos_azimut / 60
segundos_decimal_azimut = segundos_azimut / 3600
azimut_decimal = grados_azimut + minutos_decimal_azimut + segundos_decimal_azimut
        
azimut_radianes = azimut_decimal * (math.pi / 180)

print("Para hallar el azimut, ingresa la latitud que forma la línea geodésica:  ")
        
grados_latitud_pregunta = float(input("Ingresa los grados (°): "))
minutos_latitud_pregunta = float(input("Ingresa los minutos ('): "))
segundos_latitud_pregunta = float(input("Ingresa los segundos (''): "))
        
minutos_decimal_latitud_pregunta = minutos_latitud_pregunta / 60
segundos_decimal_latitud_pregunta = segundos_latitud_pregunta / 3600
latitud_decimal_pregunta = grados_latitud_pregunta + minutos_decimal_latitud_pregunta + segundos_decimal_latitud_pregunta
        
latitud_radianes_pregunta = latitud_decimal_pregunta * (math.pi / 180)


N_ej = a/(1-e*(math.sin(latitud_radianes)**2)**(1/2))
k = N_ej*(math.cos(latitud_radianes))*(math.sin(azimut_radianes))

azimut_pregunta_rad = math.asin((k*((1-e*(math.sin(latitud_radianes_pregunta ))*2)**(1/2)))/(a*(math.cos(latitud_radianes_pregunta ))))
azimut_pregunta_gr = azimut_pregunta_rad*(180/math.pi)

grados_azimut_pregunta = int(azimut_pregunta_gr)
minutos_azimut_pregunta_gr = (azimut_pregunta_gr - grados_azimut_pregunta) * 60
minutos_azimut_pregunta = int(minutos_azimut_pregunta_gr)
segundos_azimut_pregunta = (minutos_azimut_pregunta_gr - minutos_azimut_pregunta) * 60

print(f"El azimut α es: {grados_azimut_pregunta}° {minutos_azimut_pregunta}' {segundos_azimut_pregunta}'' lo que es igual a {azimut_pregunta_gr}")