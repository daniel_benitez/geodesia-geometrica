import math

print("Por favor escoge una opción :")

while True:
    print("1. Hallar el azimut de una sección normal, conociendo a, e y la latitud (φ)")

    opcion = input("Selecciona una opción: ")

    if opcion == "1":
        print("Ingresa la latitud (φ) dada: ")
        
        grados = float(input("Ingresa los grados (°): "))
        minutos = float(input("Ingresa los minutos ('): "))
        segundos = float(input("Ingresa los segundos (''): "))
        
        minutos_decimal = minutos / 60
        segundos_decimal = segundos / 3600
        latitud_decimal = grados + minutos_decimal + segundos_decimal
        
        latitud_radianes = latitud_decimal * (math.pi / 180)
        
        a = 6378388
        e = 0.006722684
        Px = float(input("Ingresa el valor de Px (radio de curvatura en el azimut α): "))
        
        RO_rad = a*(1-e)/(1-e*(math.sin(latitud_radianes))**2)**(3/2)
        N_rad = a/(1-e*(math.sin(latitud_radianes))**2)**(1/2)
                        
        print(f"        El valor de la sección normal es    N =     {N_rad}")
        print(f"        El valor del radio de curvatura es  P =     {RO_rad}")
        
        α_rad = math.acos(math.sqrt((RO_rad*(N_rad-Px))/(Px*(N_rad-RO_rad))))
        α_grados = α_rad * (180/math.pi)
        
        grados = int(α_grados)
        minutos_decimal = (α_grados - grados) * 60
        minutos = int(minutos_decimal)
        segundos = (minutos_decimal - minutos) * 60
       
        print(f"        La latitud (φ) es: {grados}° {minutos}' {segundos}'' lo que es igual a {latitud_decimal} en grados decimales") 
        print(f"        a = {a}")
        print(f"        e = {e}")
        print(f"        El azimut (α) es: {grados}° {minutos}' {segundos}'' lo que es igual a {α_grados} en grados decimales")

        break

    else:
        print("Opción no válida. Por favor, selecciona una opción válida.")
