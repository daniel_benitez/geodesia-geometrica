import math

print("Por favor escoge una opción :")

while True:
    print("1. Hallar la posición Y,Z conociendo a, b y la latitud (φ)")
    print("2. Hallar la posición Y,Z conociendo a, f y la latitud (φ)")
    print("3. Hallar la posición Y,Z conociendo a, e y la latitud (φ)")

    opcion = input("Selecciona una opción: ")

    if opcion == "1":
        print("Ingresa la latitud (φ) dada: ")
        
        grados = float(input("Ingresa los grados (°): "))
        minutos = float(input("Ingresa los minutos ('): "))
        segundos = float(input("Ingresa los segundos (''): "))
        
        minutos_decimal = minutos / 60
        segundos_decimal = segundos / 3600
        latitud_decimal = grados + minutos_decimal + segundos_decimal
        
        latitud_radianes = latitud_decimal * (math.pi / 180)
        
        a = float(input("Ingresa el valor de a (semi-eje mayor): "))
        b = float(input("Ingresa el valor de b (semi-eje menor): "))
        
        e = 1-(b**2/a**2)
        
        y_rad = (a*math.cos(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        z_rad = (a*(1-e)*math.sin(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        
        print(f"        La latitud (φ): {grados}° {minutos}' {segundos}\" es igual a {latitud_decimal} en grados decimales") 
        print(f"        a = {a}")
        print(f"        b = {b}")
        print(f"        e = {e}")                
        print(f"        La posición Y es {y_rad}")
        print(f"        La posición Z es {z_rad}")
        
        break
        
    elif opcion == "2":
        
        print("Ingresa la latitud (φ) dada: ")
        
        grados = float(input("Ingresa los grados (°): "))
        minutos = float(input("Ingresa los minutos ('): "))
        segundos = float(input("Ingresa los segundos (''): "))
        
        minutos_decimal = minutos / 60
        segundos_decimal = segundos / 3600
        latitud_decimal = grados + minutos_decimal + segundos_decimal
        
        latitud_radianes = latitud_decimal * (math.pi / 180)
        
        a = float(input("Ingresa el valor de a (semi-eje mayor): "))
        
        entrada_fraccional = input("Ingresa f (achatamiento polar) en formato fraccional: ")

        numerador_str, denominador_str = entrada_fraccional.split('/')
        numerador = float(numerador_str)
        denominador = float(denominador_str)

        f = numerador / denominador
      
        b = a-(a*f)
        
        e = 1-(b**2/a**2)
        
        y_rad = (a*math.cos(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        z_rad = (a*(1-e)*math.sin(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        
        print(f"        La latitud (φ): {grados}° {minutos}' {segundos}\" es igual a {latitud_decimal} en grados decimales") 
        print(f"        a = {a}")
        print(f"        b = {b}")
        print(f"        e = {e}")                
        print(f"        La posición Y es {y_rad}")
        print(f"        La posición Z es {z_rad}")
        
        break
    
    elif opcion == "3":
        
        print("Ingresa la latitud (φ) dada: ")
        
        grados = float(input("Ingresa los grados (°): "))
        minutos = float(input("Ingresa los minutos ('): "))
        segundos = float(input("Ingresa los segundos (''): "))
        
        minutos_decimal = minutos / 60
        segundos_decimal = segundos / 3600
        latitud_decimal = grados + minutos_decimal + segundos_decimal
                
        latitud_radianes = latitud_decimal * (math.pi / 180)
        
        a = float(input("Ingresa el valor de a (semi-eje mayor): "))
        e = float(input("Ingresa el valor de e (excentricidad): "))
    
        y_rad = (a*math.cos(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        z_rad = (a*(1-e)*math.sin(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
       
        print(f"        La latitud (φ): {grados}° {minutos}' {segundos}\" es igual a {latitud_decimal} en grados decimales") 
        print(f"        a = {a}")
        print(f"        e = {e}")                
        print(f"        La posición Y es {y_rad}")
        print(f"        La posición Z es {z_rad}")
        
        break
    
    else:
        print("Opción no válida. Por favor, selecciona una opción válida.")
