import math

a = 6378388
e = 0.006722684

print("Ingresa la latitud (φ) dada en el ejercicio: ")
        
grados_latitud = float(input("Ingresa los grados (°): "))
minutos_latitud = float(input("Ingresa los minutos ('): "))
segundos_latitud = float(input("Ingresa los segundos (''): "))
        
minutos_decimal_latitud = minutos_latitud / 60
segundos_decimal_latitud = segundos_latitud / 3600
latitud_decimal = grados_latitud + minutos_decimal_latitud + segundos_decimal_latitud
        
latitud_radianes = latitud_decimal * (math.pi / 180)

print("Ingresa el azimut (α) dado en el ejercicio: ")
        
grados_azimut = float(input("Ingresa los grados (°): "))
minutos_azimut = float(input("Ingresa los minutos ('): "))
segundos_azimut = float(input("Ingresa los segundos (''): "))
        
minutos_decimal_azimut = minutos_azimut / 60
segundos_decimal_azimut = segundos_azimut / 3600
azimut_decimal = grados_azimut + minutos_decimal_azimut + segundos_decimal_azimut
        
azimut_radianes = azimut_decimal * (math.pi / 180)


print("Para hallar la latitud, ingresa el azimut que forma la línea geodésica:  ")
        
grados_azimut_pregunta = float(input("Ingresa los grados (°): "))
minutos_azimut_pregunta = float(input("Ingresa los minutos ('): "))
segundos_azimut_pregunta = float(input("Ingresa los segundos (''): "))
        
minutos_decimal_azimut_pregunta = minutos_azimut_pregunta / 60
segundos_decimal_azimut_pregunta = segundos_azimut_pregunta / 3600
azimut_decimal_pregunta = grados_azimut_pregunta + minutos_decimal_azimut_pregunta + segundos_decimal_azimut_pregunta
        
azimut_radianes_pregunta = azimut_decimal_pregunta * (math.pi / 180)

k = ((a*(math.cos(latitud_radianes))*(math.sin(azimut_radianes)))/(1-e*((math.sin(latitud_radianes))**2)**(1/2)))/(math.sin(azimut_radianes_pregunta))
k_gr=k*(180/math.pi)

latitud_pregunta_rad = math.acos(math.sqrt(((k**2)*(1-e))/((a**2)-((k**2)*(e)))))
latitud_pregunta_gr = latitud_pregunta_rad*(180/math.pi)

print(f"La latitud es: {latitud_pregunta_gr}")

