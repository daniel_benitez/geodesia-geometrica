import math

print("Por favor escoge una opción :")

while True:
    print("1. Hallar la posición X,Y,Z conociendo a, el elipsoide, latitud (φ) y longitud (λ)")
    print("2. Hallar la posición X,Y,Z conociendo ")

    opcion = input("Selecciona una opción: ")

    if opcion == "1":
        
        #LATITUD
        print("Ingresa la latitud (φ) dada: ")
        grados_lat = float(input("Ingresa los grados (°): "))
        minutos_lat = float(input("Ingresa los minutos ('): "))
        segundos_lat = float(input("Ingresa los segundos (''): "))
        minutos_decimal_lat = minutos_lat / 60
        segundos_decimal_lat = segundos_lat / 3600
        latitud_decimal = grados_lat + minutos_decimal_lat + segundos_decimal_lat
        latitud_radianes = latitud_decimal * (math.pi / 180)
        
        #LONGITUD
        print("Ingresa la longitud (λ) dada: ")
        grados_lon = float(input("Ingresa los grados (°): "))
        minutos_lon = float(input("Ingresa los minutos ('): "))
        segundos_lon = float(input("Ingresa los segundos (''): "))
        minutos_decimal_lon = minutos_lon / 60
        segundos_decimal_lon = segundos_lon / 3600
        longitud_decimal = grados_lon + minutos_decimal_lon + segundos_decimal_lon
        longitud_radianes = longitud_decimal * (math.pi / 180)
        
        #ENTRADA DE a
        a = float(input("Ingresa el valor de a (semi-eje mayor): "))
        
        #ENTRADA DE f
        entrada_fraccional = input("Ingresa f (achatamiento polar) en formato fraccional: ")
        numerador_str, denominador_str = entrada_fraccional.split('/')
        numerador = float(numerador_str)
        denominador = float(denominador_str)
        f = numerador / denominador

        #CALCULO DE b
        b = a-(a*f)
        
        #CALCULO DE e
        e = 1-(b**2/a**2)
        
        #CALCULO DE COORDENADAS
        x_rad = (a*math.sin(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        y_rad = (a*math.cos(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        z_rad = (a*(1-e)*math.sin(latitud_radianes))/(math.sqrt((1-e*((math.sin(latitud_radianes))**2))))
        
        print(f"        La latitud: {grados_lat}° {minutos_lat}' {segundos_lat}\" es igual a {latitud_decimal} en grados decimales")
        print(f"        La longitud: {grados_lon}° {minutos_lon}' {segundos_lon}\" es igual a {longitud_decimal} en grados decimales") 
        print(f"        a = {a}")
        print(f"        b = {b}")
        print(f"        e = {e}")
        print(f"        La posición X es {x_rad}")                
        print(f"        La posición Y es {y_rad}")
        print(f"        La posición Z es {z_rad}")
        
        break
    
    elif opcion == "2":
        
        
        
        break    
    
    else:
        print("Opción no válida. Por favor, selecciona una opción válida.")
